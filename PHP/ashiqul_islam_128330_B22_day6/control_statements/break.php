<?php
// It stop the loop execution when it 
// faces or fulfill the condition

$a = array('Shaon', 'Nadim', 'Ashiqul Islam');

foreach ($a as $key => $name) {
    if ($name == 'Nadim') {
        break;
    }
    echo $key. " => " . $name. " <br>";
}

// Output
        // Shaon