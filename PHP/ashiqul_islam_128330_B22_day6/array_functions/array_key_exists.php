<?php


//array_key_exists — Checks if the given key or index exists in the array

$a = ['a' => 'shaon', 'b' => 'ball'];

if(array_key_exists('a',$a ))
    echo "shaon value stays with the a key";