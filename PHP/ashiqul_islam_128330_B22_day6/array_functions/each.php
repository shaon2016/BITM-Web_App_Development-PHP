<?php

// Need to understand

///each — Return the current key and
// value pair from an array and advance the array cursor


//After each() has executed, the array cursor
// will be left on the next element of the array,
// or past the last element if it hits the end of
// the array. You have to use reset() if you want to
// traverse the array again using each.

$foo = array("bob", "fred", "jussi", "jouni", "egon", "marliese");
$bar = each($foo);
print_r($bar);

// Output

//Array
//(
//    [1] => bob
//    [value] => bob
//[0] => 0
//    [key] => 0
//)