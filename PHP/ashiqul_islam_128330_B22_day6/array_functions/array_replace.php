<?php

//array_replace — Replaces elements from passed
// arrays into the first array

$a = [2,3];
$b = [2,5,6,8];

$result = array_replace($a, $b);

echo "<pre>";
print_r($result);
echo "</pre>";

// Output

/*
 * Array
(
    [0] => 2
    [1] => 5
    [2] => 6
)
 */

echo "<hr>";




$a = ['a' => 2, 'b' => 3];
$b = [2,5,6,8];

$result = array_replace($a, $b);

echo "<pre>";
print_r($result);
echo "</pre>";




echo "<hr>";


$a = ['a' => 2, 'b' => 3];
$b = ['a' => 56,5,6,8, 'c' => 87];

$result = array_replace($a, $b);

echo "<pre>";
print_r($result);
echo "</pre>";









