<?php

//end — Set the internal pointer of an array to its last element

$a = [2,3,4,5];

echo current($a)."<br>"; // 2

echo next($a)."<br>"; // 3

echo current($a)."<br>"; // 3

echo end($a)."<br>"; // 5