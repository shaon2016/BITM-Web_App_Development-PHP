<?php

//array_reverse — Return an array with elements in reverse order

$a = [2,3,4,5,6];

$reverse_result = array_reverse($a);

echo "<pre>";
var_dump($reverse_result);
echo "</pre>";