<?php

// 	TRUE if $a is equal to $b, and they are of the same type.

$a = $b = 10;


if ($a === $b)
    echo "a is equal to b and identical". "<hr>";
else
    echo " a is not equal to b or not identical". "<hr>";

$a = 3;
$b = 2.2;


if ($a === $b)
    echo "a is equal to b and identical". "<hr>";
else
    echo " a is not equal to b or not identical". "<hr>";

$a = 3;
$b = 4;


if ($a === $b)
    echo "a is equal to b and identical". "<hr>";
else
    echo " a is not equal to b or not identical". "<hr>";