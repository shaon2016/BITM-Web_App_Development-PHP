<?php
//explode — Split a string by string

$name = "Ashiqul Islam";

// explode(delimeter, string)
// it splits the string using delemeter
//we use a string that is used in the string
// and then split the string from delimeter position

echo "<pre>";
print_r(explode(" ", $name));
echo "</pre>";
echo "<hr>";

// It will split the string when it face the comma
$name = "ashiq, shaon, nadim";
echo "<pre>";
print_r(explode(",", $name));
echo "</pre>";

echo "<hr>";


// Limit checking

$str = 'one|two|three|four';

// positive limit
echo "<pre>";
print_r(explode('|', $str, 2));
echo "</pre>";
echo "<hr>";
// negative limit (since PHP 5.1)
echo "<pre>";
print_r(explode('|', $str, -1));
echo "</pre>";
echo "<hr>";
?>