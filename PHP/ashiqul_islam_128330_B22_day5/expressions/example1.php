<?php
/*This is the easy part of our expression example*/
$p = 2;
$q = 4;

echo $p + $q;
echo "<hr>";

$p = $q = 5;

echo $p;
echo "<hr>";
echo $q;
echo "<hr>";

$p++;
echo "The value of p ". $p; // using post increment
echo "<hr>";
echo "pre increment value of p is ". ++$p;
echo "<hr>";

$p = $q = 3;

// Same precedence. Operator will from right to left
echo ++$p + ++$p + $q++ ; // 4 + 5 + 3
echo "<hr>";