<?php

$a = 3.3;
echo "$a"." and it is ".gettype($a)."<hr>";

$a = (int) $a;
echo "$a"." and it is ".gettype($a)."<hr>";

$a = (string) $a;
echo "$a"." and it is ".gettype($a)."<hr>";
