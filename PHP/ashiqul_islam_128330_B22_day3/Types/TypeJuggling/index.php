<?php

$a = "2";
$b = 3;
// $a may be string. but in php
// when it faces operation like + / *
// It will act like integer or float or double
echo $a + $b. "<hr>";

$b = "3";
echo $a + $b. "<hr>";

$a = "2 glass of water";
$b = 2.3;
// php will pick the first string as integer and later
// string will ignore
echo $a+$b. "<hr>";
// If it faces string at first it will ignore the whole
$a = "Give me 2 glass";
$b = 2.3;

echo $a+$b;